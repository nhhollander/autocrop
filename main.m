% Get the working directory
work_dir = uigetdir(pwd,'Select Working Directory');
% Infinite Loop
while 1
    % Find input files
    files = dir(sprintf('%s/**/input_*.png',work_dir));
    % Check if file
    if isempty(files)
        disp('No scan ready, waiting...')
        pause(5);
        continue
    end
    % Get the file name
    fname = files(1).name;
    folder = files(1).folder;
    % Check if file is open
    [~,test] = system(sprintf('lsof | grep %s', fname));
    if(~isempty(test))
        disp('Scan in progress, waiting...');
        pause(5);
        continue;
    end
    fprintf('Processing File %s/%s\n', folder, fname)
    % Process the file
    autocrop(sprintf('%s/%s',folder,fname), files(1).folder);
    % Delete the file
    delete(sprintf('%s/%s',folder,fname));
end