function autocrop(fname, output_folder)
% Ignore image display warnings
warning('off', 'Images:initSize:adjustingMag');

% Image scale factor
image_scale_factor = 0.1;
% Image size threshold (percent)
image_size_thresh = 0.01;
% Image fill threshold (ratio)
image_fill_thresh = 0.7;

disp('Loading Image')
image = imread(fname);
% Resize the image
disp('Resizing Image')
image_r = imresize(image, image_scale_factor);
[reduced_height, reduced_width] = size(image_r);
imshow(image_r);

% Send the image to the GPU
gpuDevice(1);
image_r = gpuArray(image_r);
% Mask off the image
disp('Initial Filtering')
bimage = mask_image(image_r);
imshow(bimage);

pause(0.25);
% Fill in regions
disp('Filling in regions')
bimage = imfill(bimage, 'holes');
imshow(bimage);
pause(0.25);
% Use erosion to remove connected dust
disp('Eroding dust');
se = strel('disk',4,4);
bimage = imerode(bimage, se);
bimage = imdilate(bimage,se);
% Get region properties
disp('Getting region properties')
props = regionprops(bimage, 'BoundingBox', 'Centroid', 'Area');
%% Debug me
subplot(1,2,1);
imshow(image);
pause(0.25);
hold on;
% Calculate the area threshold
area_threshold = reduced_width * reduced_height * image_size_thresh;
for i = 1:length(props)
    % Check for tiny areas
    if props(i).Area < area_threshold
        continue;
    end
    % Check for sparse regions (almost always misidentified blobbies)
    bounding_box = props(i).BoundingBox;
    bbwidth = bounding_box(3);
    bbheight = bounding_box(4);
    bbarea = bbwidth * bbheight;
    area_ratio = props(i).Area / bbarea;
    if area_ratio < image_fill_thresh
        continue;
    end
    % Reverse scale factor
    rsf = 1 / image_scale_factor;
    % Scale the bounding box
    bounding_box_s = [bounding_box(1) * rsf, bounding_box(2) * rsf, ...
        bounding_box(3) * rsf, bounding_box(4) * rsf];
    % Reset to main plot for debug point drawing
    subplot(1,2,1);
    rectangle('Position', bounding_box_s, 'EdgeColor', 'r');
    plot(props(i).Centroid(1) * rsf, props(i).Centroid(2) * rsf, '+r', 'MarkerSize', 30, 'LineWidth', 2);
    % Switch to subplot for image process output
    subplot(1,2,2);
    % Expand the crop zone
    bounding_box_s(1) = bounding_box_s(1) - 100;
    bounding_box_s(2) = bounding_box_s(2) - 100;
    bounding_box_s(3) = bounding_box_s(3) + 200;
    bounding_box_s(4) = bounding_box_s(4) + 200;
    % Crop the image
    disp('Cropping Image')
    cropped_image = imcrop(image, bounding_box_s);
    imshow(cropped_image);
    pause(0.25);
    % Crop the bw image
    cropped_bimage = imcrop(bimage, bounding_box);
    % Calculate edges
    disp('Calculating Rotation')
    edges = gather(edge(cropped_bimage, 'sobel'));
    % Compute the hough transform
    [H,theta,rho] = hough(edges);
    % Locate peaks in the image
    P = houghpeaks(H,5,'threshold',ceil(0.3*max(H(:))));
    % Find lines
    lines = houghlines(gather(cropped_bimage),theta,rho,P,'FillGap',50,'MinLength',50);
    
    % Calculate angles
    asum = 0;
    acount = 0;
    for k = 1:length(lines)
       xy = [lines(k).point1; lines(k).point2];
       % Get delta x
       dx = xy(2,1) - xy(1,1);
       dy = xy(2,2) - xy(1,2);
       angle = rad2deg(atan(dy / dx));
       if angle > 45
           angle = angle - 90;
       end
       if angle < -45
           angle = angle + 90;
       end
       asum = asum + angle;
       acount = acount + 1;
    end
    angle = asum / acount;
    % Clear some memory
    clear H edges rho lines theta
    % Rotate the image
    disp('Straightening Image')
    cropped_image = imrotate(cropped_image, angle);
    imshow(cropped_image);
    pause(0.25);
    % Send the image to the GPU
    %cropped_image = gpuArray(cropped_image);
    % Filter the image again
    disp('Filtering image')
    cropped_bimage = mask_image(cropped_image);
    % Get area of image
    cropped_bimage_size = size(cropped_bimage);
    cropped_image_area = cropped_bimage_size(1) * cropped_bimage_size(2);
    % Fill in holes
    cropped_bimage = imfill(cropped_bimage, 'holes');
    % Erode the dust
    disp('Eroding dust');
    se = strel('disk',6,6);
    cropped_bimage = imerode(cropped_bimage, se);
    cropped_bimage = imdilate(cropped_bimage,se);
    % Remove islands
    disp('Cleaning up imagecropped_image')
    cropped_bimage = bwareaopen(cropped_bimage, round(cropped_image_area * 0.1), 4);
    % Get image properties
    disp('Getting image properties')
    crop_prop = regionprops(cropped_bimage, 'BoundingBox');
    % Crop the image again
    disp('Final Crop');
    cropped_image = imcrop(cropped_image, crop_prop.BoundingBox);
    imshow(cropped_image);
    pause(0.25);
    
    % Save that bad boy
    disp('Saving Image')
    % Determine the file name
    files = dir(sprintf('%s/im_*.png', output_folder));
    highest = 0;
    for file = 1:length(files)
        num = str2double(files(file).name(4:7));
        if(num > highest)
            highest = num;
        end
    end
    % File name
    fname = sprintf('%s/im_%04i.png', output_folder, highest + 1);
    % Write the file
    imwrite(cropped_image, fname);
end

hold off;
% Clear everything out
clear;
gpuArray(1);

% Done
disp('Done!')

end
