#!/bin/bash

scanimage -d 'imagescan:esci:gt-s650:usb:/sys/devices/pci0000:00/0000:00:14.0/usb1/1-2/1-2:1.0' --resolution 1500 --mode Color --scan-area Maximum --format png --batch=input_%04d.png --batch-count=100 --batch-prompt --progress #2> /dev/null
