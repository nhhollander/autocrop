function corners = getcorners(image, bounds)

    % Scan Limits
    ymin = bounds(2);
    ymax = bounds(2) + bounds(4);
    xmin = bounds(1);
    xmax = bounds(1) + bounds(3);
    
    ymin = round(ymin);
    ymax = round(ymax);
    xmin = round(xmin);
    xmax = round(xmax);
        
    % Points
    upper_left = [0 0];
    upper_right = [0 0];
    lower_left = [0 0];
    lower_right = [0 0];
    
    % Distances
    upper_left_d = 10000;
    upper_right_d = 10000;
    lower_left_d = 10000;
    lower_right_d = 10000;
        
    % Find the upper left bound
    for y = ymin:(ymin + 100)
        for x = xmin:(xmin + 100)
            % Check if light pixel
            if image(y, x) == 0
                continue;
            end
            % Check distance
            dmatrix = [xmin, ymin; x, y];
            dist = pdist(dmatrix);
            if dist < upper_left_d
                upper_left_d = dist;
                upper_left = [x y];
            end
        end
    end
        
    % Find the upper right bound
    for y = ymin:(ymin + 100)
        for x = (xmax - 100):xmax
            % Check if light pixel
            if image(y, x) == 0
                continue;
            end
            % Check distance
            dmatrix = [xmax, ymin; x, y];
            dist = pdist(dmatrix);
            if dist < upper_right_d
                upper_right_d = dist;
                upper_right = [x y];
            end
        end
    end
    
    % Fuck the other distances
    
    % Done
    corners = [upper_left; upper_right; lower_left; lower_right];

end